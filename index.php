<!doctype html>
<html lang="es-ES">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/x-icon" href="assets/img/girasol.svg" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet"/>
    <!-- Google fonts-->
    <link rel="preconnect" href="https://fonts.gstatic.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&amp;display=swap"
          rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&amp;display=swap"
          rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&amp;display=swap"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/style.css" rel="stylesheet"/>

    <title>Digitalización Empresas</title>
</head>
    <body id="page-top">

        <nav class="navbar navbar-expand-lg navbar-light fixed-top shadow-sm" id="mainNav">
            <div class="container px-5">
                <a class="navbar-brand fw-bold" href="#page-top">
                    <img src="assets/img/logo.svg" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="bi-list"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto me-4 my-3 my-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-bs-toggle="dropdown" aria-expanded="false">
                                Desarrollo
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#software">Software</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="#web">Web design</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#branding">Branding</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#marketing">Marketing</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#contacto">Contacto</a>
                        </li>


                    </ul>
                    <button class="btn btn-primary rounded-pill px-3 mb-2 mb-lg-0" data-bs-toggle="modal"
                            data-bs-target="#feedbackModal">
                                <span class="d-flex align-items-center">
                                    <i class="bi bi-whatsapp me-2"></i>
                                    <span class="small"><a href="https://api.whatsapp.com/send?phone=34696270847&text=Estoy%20interesado%20en%20saber..." class="text-light text-decoration-none">Contacto directo</a></span>
                                </span>
                    </button>
                </div>
            </div>
        </nav>

        <!-- Mashead header-->
        <header class="masthead">
            <div class="container px-5">
                <div class="row gx-5 align-items-center">
                    <div class="col-lg-6">
                        <!-- Mashead text and app badges-->
                        <div class="mb-5 mb-lg-0 text-center text-lg-start">
                            <h1 class="display-1 lh-1 mb-3">Digitalización para tu empresa.</h1>
                            <p class="lead fw-normal text-muted mb-5">Eleva tu empresa a un nuevo nivel e interactua con
                                clientes de todo el país y el mundo</p>
                            <div class="d-flex flex-column flex-lg-row align-items-center">

                                <button class="btn btn-primary rounded-pill px-3 mb-2 mb-lg-0" data-bs-toggle="modal"
                                        data-bs-target="#feedbackModal">
                                <span class="d-flex align-items-center">
                                    <i class="bi bi-phone me-3"></i>
                                    <span class="small">
                                        <a href="tel:+34696270847" class="text-light text-decoration-none">Cuéntame mas</a></span>
                                </span>
                                </button>
                                <!--                            <a class="me-lg-3 mb-4 mb-lg-0" href="#!"><img class="app-badge" src="assets/img/google-play-badge.svg" alt="..." /></a>-->
                                <!--                            <a href="#!"><img class="app-badge" src="assets/img/app-store-badge.svg" alt="..." /></a>-->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <!-- Masthead device mockup feature-->
                        <div class="masthead-device-mockup">
                            <svg class="circle" viewBox="0 0 100 100" xmlns="https://www.w3.org/2000/svg">
                                <defs>
                                    <linearGradient id="circleGradient" gradientTransform="rotate(45)">
                                        <stop class="gradient-start-color" offset="0%"></stop>
                                        <stop class="gradient-end-color" offset="100%"></stop>
                                    </linearGradient>
                                </defs>
                                <circle cx="50" cy="50" r="50"></circle>
                            </svg
                            >
                            <svg class="shape-1 d-none d-sm-block" viewBox="0 0 240.83 240.83"
                                 xmlns="https://www.w3.org/2000/svg">
                                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03"
                                      transform="translate(120.42 -49.88) rotate(45)"></rect>
                                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03"
                                      transform="translate(-49.88 120.42) rotate(-45)"></rect>
                            </svg
                            >
                            <svg class="shape-2 d-none d-sm-block" viewBox="0 0 100 100" xmlns="https://www.w3.org/2000/svg">
                                <circle cx="50" cy="50" r="50"></circle>
                            </svg>
                            <div class="device-wrapper">
                                <div class="device" data-device="iPhoneX" data-orientation="portrait" data-color="black">
                                    <div class="screen bg-black">
                                        <!-- PUT CONTENTS HERE:-->
                                        <!-- * * This can be a video, image, or just about anything else.-->
                                        <!-- * * Set the max width of your media to 100% and the height to-->
                                        <!-- * * 100% like the demo example below.-->
                                        <video muted="muted" autoplay="" loop="" style="max-width: 100%; height: 100%">
                                            <source src="assets/img/demo-screen.mp4" type="video/mp4"/>
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- Quote/testimonial aside-->
        <aside class="text-center bg-gradient-primary-to-secondary">
            <div class="container px-5">
                <div class="row gx-5 justify-content-center">
                    <div class="col-xl-8">
                        <div class="h2 fs-1 text-white mb-5">"¡ Soluciones intuitivas y a medida para los problemas comunes que
                            enfrenta las empresas cada día !"
                        </div>
                        <img src="assets/img/girasol-blanco.svg" alt="Digitalización para empresas y autónomos" style="height: 5rem"/>
                        <p class="text-light mt-2">Integra Design, Soluciones digitales <br> para la nueva era empresarial</p>
                    </div>
                </div>
            </div>
        </aside>

        <!-- Software section-->
        <section class="bg-light" id="software">
            <div class="container px-5">
                <div class="row gx-5 align-items-center justify-content-center justify-content-lg-between">
                    <div class="col-12 col-lg-5">
                        <h2 class="display-4 lh-1 mb-4">El software que tu empresa necesita</h2>
                        <p class="mb-2">Faciltamos tu trabajo con software de ultima generación, entornos web, rápidos y que te
                            permite conectar con clientes de todo el mundo y conocer la situación de tu empresa en tiempo
                            real.</p>
                        <p class="lead fw-normal text-muted mb-5 mb-lg-0">
                        <ul>
                            <li>Software a medida</li>
                            <li>CRM</li>
                            <li>ERP</li>
                            <li>Facturación</li>
                            <li>Entre otros</li>
                        </ul>
                    </div>
                    <div class="col-sm-8 col-md-6">
                        <div class="px-5 px-sm-0"><img class="img-fluid"
                                                       src="assets/img/desarrollo-de-software.png" alt="..."/></div>
                    </div>
                </div>
            </div>
        </section>

        <!--  Web Development section-->
        <section class="bg-dark" id="web">
            <div class="container px-5">
                <div class="row gx-5 align-items-center justify-content-center justify-content-lg-between">

                    <div class="col-sm-8 col-md-6">
                        <div class="px-5 px-sm-0"><img class="img-fluid"
                                                       src="assets/img/desarrollo-web-integra.png" alt="desarrollo web integra"/></div>
                    </div>

                    <div class="col-12 col-lg-5">
                        <h2 class="display-4 lh-1 mb-4 text-light">Nextgen Webs, Sitios web de nueva generación</h2>
                        <p class="mb-2 text-light">Sitios web de nueva generación, tanto si tienes una web corporativa o una
                            tienda online. Nuestras asesorías te permitirán tomar decisiones más certeras para la creación de tu
                            sitio web. </p>
                        <p class="lead fw-normal text-muted mb-5 mb-lg-0 ">
                            <ul class="text-light">
                                <li>Desarrollo web a medida</li>
                                <li>Diseño web</li>
                                <li>Comercio electrónico</li>
                                <li>Aulas virtuales</li>
                                <li>También Wordpress, Drupal, Joomla</li>
                            </ul>
                        </p>
                    </div>

                </div>
            </div>
        </section>

        <!-- Branding section-->
        <section class="bg-light" id="branding">
            <div class="container px-5">
                <div class="row gx-5 align-items-center justify-content-center justify-content-lg-between">
                    <div class="col-12 col-lg-5">
                        <h2 class="display-4 lh-1 mb-4">Tu marca cruzando el oceano.</h2>
                        <p>Más alcance para tu marca. Preparamos todo lo necesario para que tu marca llegue más lejos. Sin
                            importar el mercado que quieras alcanzar, nosotros te ayudamos a generar la marca que impacte en
                            dicho mercado.
                        </p>
                        <p class="lead fw-normal text-muted mb-5 mb-lg-0">
                        <ul>
                            <li>Diseño grafico corporativo</li>
                            <li>Logotipos</li>
                            <li>Papelería corporativa</li>
                            <li>Publicidad</li>
                        </ul>
                    </div>
                    <div class="col-sm-8 col-md-6">
                        <div class="px-5 px-sm-0"><img class="img-fluid"
                                                       src="assets/img/branding.webp" alt="Branding"/></div>
                    </div>
                </div>
            </div>
        </section>

        <!--  Marketing-->
        <section class="bg-dark" id="marketing">
            <div class="container px-5">
                <div class="row gx-5 align-items-center justify-content-center justify-content-lg-between">

                    <div class="col-sm-8 col-md-6">
                        <div class="px-5 px-sm-0"><img class="img-fluid"
                                                       src="assets/img/marketing digital.png" alt="marketing-digital"/></div>
                    </div>

                    <div class="col-12 col-lg-5">
                        <h2 class="display-4 lh-1 mb-4 text-light">Porque el alcance de tu negocio es importante</h2>
                        <p>Posicionarte en internet favorece el crecimiento de tu empresa. Las operaciones diarias en internet
                            superan los 3 billones de dólares, de los cuales las mayores ganancias se van para aquellos que
                            parecen en los primeros lugares de búsqueda. Y t en que lugar quieres aparecer ?</p>
                        <p class="lead fw-normal text-muted mb-5 mb-lg-0 ">
                        <ul class="text-light">
                            <li>Posicionamiento web (SEO)</li>
                            <li>Campañas Adwords</li>
                            <li>Campañas Redes Sociales</li>
                            <li>Gestión de redes sociales</li>
                            <li>y mucho más.</li>
                        </ul>
                        </p>
                    </div>

                </div>
            </div>
        </section>

        <!--  Contacto-->
        <section class="bg-light" id="contacto">
            <div class="container px-5">
                <div class="row gx-5 align-items-center justify-content-center justify-content-lg-between">

                    <div class="col-sm-8 col-md-6">
                        <form class="row g-3" method="post" action="controllers/mail.php" enctype="application/x-www-form-urlencoded">

                            <input type="hidden" name="feedback" value="false">
                            <input type="hidden" name="form-contact" value="landing">

                            <div class="col-md-6">
                                <label for="inputName" class="form-label">Nombre</label>
                                <input type="text" class="form-control" id="inputName" name="inputName" placeholder="Juan Pérez">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail" class="form-label">Correo Electrónico</label>
                                <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="email@tuemail.com">
                            </div>
                            <div class="col-12">
                                <label for="inputPhone" class="form-label">Teléfono</label>
                                <input type="text" class="form-control" id="inputPhone" name="inputPhone" placeholder="+00 123 456 789">
                            </div>
                            <div class="col-md-6">
                                <label for="inputCity" class="form-label">Ciudad</label>
                                <input type="text" class="form-control" id="inputCity" name="inputCity" placeholder="New York, Madrid, Bogotá">
                            </div>
                            <div class="col-md-6">
                                <label for="inputCountry" class="form-label">País</label>
                                <input type="text" class="form-control" id="inputCountry" name="inputCountry"
                                       placeholder="España, Colombia, Estados Unidos">
                            </div>

                            <div class="col-md-12">
                                <label for="areaMessage" class="form-label">Mensaje</label>
                                <textarea name="areaMessage" id="areaMessage"  placeholder="Escribe tu mensaje." class="form-control"></textarea>
                            </div>

                            <div class="col-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="gridCheck">
                                    <label class="form-check-label" for="gridCheck">
                                        He leído, acepto y entiendo la <a href="#" data-bs-toggle="modal" data-bs-target="#rgpd"> Política de privacidad</a>
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="gridCheck">
                                    <label class="form-check-label" for="gridCheck">
                                        Si, acepto recibir comunicaciones informativas y publicidad de Integra Design.
                                    </label>
                                </div>

                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </form>

                    </div>

                    <div class="col-12 col-lg-5">
                        <h2 class="display-4 lh-1 mb-4 ">¿ Necesitas más ?</h2>
                        <p>Cada empresa tiene un grado de digitalización diferente y cada una requiere una atención
                            personalizada y un desarrollo e implantación distintas. Consúltanos sin ningún compromiso y pregunta
                            por lo que necesitas. </p>
                        <p class="lead fw-normal text-muted mb-5 mb-lg-0 ">

                        </p>
                    </div>

                </div>
            </div>
        </section>

        <!-- Contact section-->
        <section class="cta">
            <div class="cta-content">
                <div class="container px-5">
                    <h2 class="text-white display-1 lh-1 mb-4">
                        Estamos listos para empezar.
                        <br/>
                        Comencemos a construir.
                    </h2>

                    <button class="btn btn-primary rounded-pill px-3 mb-2 mb-lg-0" data-bs-toggle="modal"
                            data-bs-target="#feedbackModal">
                                <span class="d-flex align-items-center">
                                    <i class="bi-chat-text-fill me-2"></i>

                                    <span class="small"><a href="+34696270847" class="text-light text-decoration-none small ">Te Escuchamos </a></span>
                                </span>
                    </button>
                </div>
            </div>
        </section>

        <!--  Marketing-->
        <section class="bg-dark p-2">
            <div class="container-fluid ">
                <div class="row align-items-center justify-content-center">
                  <div class="col-md-12">
                      <p class="text-light text-center">&copy; Todos los derechos reservados, Integra Design 2022</p>
                  </div>
                </div>
            </div>
        </section>

<!--        Cookies-->
        <div class="cookies" id="cookies">

            <div class="card-header d-flex flex-row justify-content-between align-items-center">
                <p class="">Hey! Mira las cookies!</p>
                <span id="close"><i class="bi bi-x-lg"></i></span>
            </div>

            <div class="card-body">
                <p class="small">Este sitio utiliza cookies propias y de terceros. La navegación continuada en el mismo implica la aceptaciópn de nuestra <a href="#" data-bs-toggle="modal" data-bs-target="#rgpd">política de privacidad</a>. Para la aceptacion explicita del uso de las cookies haz clic en el boton de aceptar en caso contrario clic en botón cancelar.
                </p>

                <div class="flex-column">
                    <form name="cookie-form">
                        <input type="hidden" name="cookie" id="cookie">
                    </form>
                    <a id="cookie-accept" role="button" class="btn btn-outline-primary m-1">Ok, acepto las cookies</a>
                    <a id="cookie-close" class="btn btn-outline-secondary m-1">No acepto</a>
                </div>
            </div>
        </div>

        <div class="whatsapp-contact " id="whatsapp-contact">
            <div class="whatsapp-description m-1">
                <p class="small justify-content-center p-2 ">¿ Dudas ? Te las resolvemos !</p>
            </div>
            <a href="https://wa.me/34696270847/?text=Hola bienvenido, encantado de que contactes con nosotros. Cuéntanos en qué podemos ayudarte ?">
                <img class="whatsapp-icon m-1"  src="assets/img/whatsapp-icon.png" alt="whatsapp-icon">
            </a>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="rgpd" tabindex="-1" aria-labelledby="r" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Política de protección de datos</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Este documento ha sido diseñado para tratamientos de datos personales de bajo riesgo de donde se deduce que el mismo no podrá ser utilizado para tratamientos de datos personales que incluyan datos personales  relativos al origen étnico o racial, ideología política religiosa o filosófica, filiación sindical, datos genéticos y biométricos, datos de salud, y datos de orientación sexual de las personas así como cualquier otro tratamiento de datos que entrañe alto riesgo para los derechos y libertades de las personas.</p>
                        <p>El artículo 5.1.f del Reglamento General de Protección de Datos (en adelante, RGPD) determina la necesidad de establecer garantías de seguridad adecuadas contra el tratamiento no autorizado o ilícito, contra la pérdida de los datos personales, la destrucción o el daño accidental. Esto implica el establecimiento de medidas técnicas y organizativas encaminadas a asegurar la integridad y confidencialidad de los datos personales y la posibilidad de demostrar, tal y como establece el artículo 5.2, que estas medidas se han llevado a la práctica (responsabilidad proactiva).</p>

                        <p>Además, deberá establecer mecanismos visibles, accesibles y sencillos para el ejercicio de derechos y tener definidos procedimientos internos para garantizar la atención efectiva de las solicitudes recibidas.</p>

                        <h5>ATENCIÓN DEL EJERCICIO DE DERECHOS</h5>
                        <p>El responsable del tratamiento informará a todos los trabajadores acerca del procedimiento para atender los derechos de los interesados, definiendo de forma clara los mecanismos por los que pueden ejercerse los derechos (medios electrónicos, referencia al Delegado de Protección de Datos si lo hubiera, dirección postal, etc.) y teniendo en cuenta lo siguiente:</p>

                        <ul>
                            <li>Previa presentación de su documento nacional de identidad o pasaporte, los titulares de los datos personales (interesados) podrán ejercer sus derechos de acceso, rectificación, supresión, oposición, portabilidad y limitación del tratamiento. El ejercicio de los derechos es gratuito.</li>
                            <li>El responsable del tratamiento deberá dar respuesta a los interesados sin dilación indebida y de forma concisa, transparente, inteligible, con un lenguaje claro y sencillo y conservar la prueba del cumplimiento del deber de responder a las solicitudes de ejercicio de derechos formuladas.</li>
                            <li>Si la solicitud se presenta por medios electrónicos, la información se facilitará por estos medios cuando sea posible, salvo que el interesado solicite que sea de otro modo.</li>
                            <li>Las solicitudes deben responderse en el plazo de 1 mes desde su recepción, pudiendo prorrogarse en otros dos meses teniendo en cuenta la complejidad o el número de solicitudes, pero en ese caso debe informarse al interesado de la prórroga en el plazo de un mes a partir de la recepción de la solicitud, indicando los motivos de la dilación.</li>
                        </ul>

                        <h6>DERECHO DE ACCESO:</h6>

                        <p>En el derecho de acceso se facilitará a los interesados copia de los datos personales de los que se disponga junto con la finalidad para la que han sido recogidos, la identidad de los destinatarios de los datos, los plazos de conservación previstos o el criterio utilizado para determinarlo, la existencia del derecho a solicitar la rectificación o supresión de datos personales así como la limitación o la oposición a su tratamiento, el derecho a presentar una reclamación ante la Agencia Española de Protección de Datos y si los datos no han sido obtenidos del interesado, cualquier información disponibles sobre su origen. El derecho a obtener copia de los datos no puede afectar negativamente a los derechos y libertades de otros interesados.</p>

                        <ul>
                            <li>Formulario para el ejercicio del derecho de acceso.</li>
                        </ul>

                        <h6>DERECHO DE RECTIFICACIÓN:</h6>
                        <p>En el derecho de rectificación se procederá a modificar los datos de los interesados que fueran inexactos o incompletos atendiendo a los fines del tratamiento. El interesado deberá indicar en la solicitud a qué datos se refiere y la corrección que haya de realizarse, aportando, cuando sea preciso, la documentación justificativa de la inexactitud o carácter incompleto de los datos objeto de tratamiento. Si los datos han sido comunicados por el responsable a otros responsables, deberá notificarles la rectificación de estos salvo que sea imposible o exija un esfuerzo desproporcionado, facilitando al interesado información acerca de dichos destinatarios, si así lo solicita.</p>

                        <ul>
                            <li>Formulario para el ejercicio del derecho de rectificación</li>
                        </ul>

                        <h6>DERECHO DE SUPRESIÓN: </h6>
                        <p>En el derecho de supresión se eliminarán los datos de los interesados cuando estos manifiesten su negativa al tratamiento y no exista una base legal que lo impida, no sean necesarios en relación con los fines para los que fueron recogidos, retiren el consentimiento prestado y no haya otra base legal que legitime el tratamiento o éste sea ilícito. Si la supresión deriva del ejercicio del derecho de oposición del interesado al tratamiento de sus datos con fines de mercadotecnia, pueden conservarse los datos identificativos del interesado con el fin de impedir futuros tratamientos. Si los datos han sido comunicados por el responsable a otros responsables, deberá notificarles la supresión de estos salvo que sea imposible o exija un esfuerzo desproporcionado, facilitando al interesado información acerca de dichos destinatarios, si así lo solicita.</p>
                        <ul>
                            <li>Formulario para el ejercicio del derecho de supresión.</li>
                        </ul>

                        <h6>DERECHO DE OPOSICIÓN:</h6>
                        <p> En el derecho de oposición, cuando los interesados manifiesten su negativa al tratamiento de sus datos personales ante el responsable, este dejará de procesarlos siempre que no exista una obligación legal que lo impida. Cuando el tratamiento esté basado en una misión de interés público o en el interés legítimo del responsable, ante una solicitud de ejercicio del derecho de oposición, el responsable dejará de tratar los datos salvo que se acrediten motivos imperiosos que prevalezcan sobre los intereses, derechos y libertades del interesado o sean necesarios para la formulación, ejercicio o defensa de reclamaciones. Si el interesado se opone al tratamiento con fines de mercadotecnia directa, los datos personales dejarán de ser tratados para estos fines.</p>
                        <ul>
                            <li>Formulario para el ejercicio del derecho de oposición.</li>
                        </ul>


                        <h6>DERECHO DE PORTABILIDAD:</h6>
                        <p>En el derecho de portabilidad, si el tratamiento se efectúa por medios automatizados y se basa en el consentimiento o se realiza en el marco de un contrato, los interesados pueden solicitar recibir copia de sus datos personales en un formato estructurado, de uso común y lectura mecánica. Asimismo, tienen derecho a solicitar que sean transmitidos directamente a un nuevo responsable, cuya identidad deberá ser comunicada, cuando sea técnicamente posible.</p>

                        <ul>
                            <li>Formulario para el ejercicio de la portabilidad de los datos.</li>
                        </ul>

                        <h6>DERECHO DE LIMITACIÓN AL TRATAMIENTO: </h6>
                        <p>En el derecho de limitación del tratamiento, los interesados pueden solicitar la suspensión del tratamiento de sus datos para impugnar su exactitud mientras el responsable realiza las verificaciones necesarias o en el caso de que el tratamiento se realice en base al interés legítimo del responsable o en cumplimiento de una misión de interés público, mientras se verifica si estos motivos prevalecen sobre los intereses, derechos y libertades del interesado. El interesado también puede solicitar la conservación de los datos si considera que el tratamiento es ilícito y, en lugar de la supresión, solicita la limitación del tratamiento, o si aun no necesitándolos ya el responsable para los fines para los que fueron recabados, el interesado los necesita para la formulación, ejercicio o defensa de reclamaciones. La circunstancia de que el tratamiento de los datos del interesado esté limitado deberá constar claramente en los sistemas del responsable. Si los datos han sido comunicados por el responsable a otros responsables, deberá notificarles la limitación del tratamiento de estos salvo que sea imposible o exija un esfuerzo desproporcionado, facilitando al interesado información acerca de dichos destinatarios, si así lo solicita.</p>
                        <ul>
                            <li>Formulario para el ejercicio de la limitación del tratamiento.</li>
                        </ul>

                        <p>Si no se da curso a la solicitud del interesado, el responsable del tratamiento le informará, sin dilación y a más tardar transcurrido un mes desde la recepción de esta, de las razones de su no actuación y de la posibilidad de presentar una reclamación ante la Agencia Española de Protección de Datos y de ejercitar acciones judiciales.</p>

                        <h6>MEDIDAS DE SEGURIDAD</h6>
                        <p> A tenor del tipo de tratamiento que ha puesto de manifiesto cuando ha cumplimentado este formulario, las medidas de seguridad mínimas que debería tener en cuenta son las siguientes:</p>

                        <h5>MEDIDAS ORGANIZATIVAS</h5>

                        <p><strong>INFORMACIÓN QUE DEBERÁ SER CONOCIDA POR TODO EL PERSONAL CON ACCESO A DATOS PERSONALES</strong></p>
                        <p>Todo el personal con acceso a los datos personales deberá tener conocimiento de sus obligaciones con relación a los tratamientos de datos personales y serán informados acerca de dichas obligaciones. La información mínima que será conocida por todo el personal será la siguiente:</p>

                        <ul>
                            <li>DEBER DE CONFIDENCIALIDAD Y SECRETO</li>
                            <li>Se deberá evitar el acceso de personas no autorizadas a los datos personales. A tal fin se evitará dejar los datos personales expuestos a terceros (pantallas electrónicas desatendidas, documentos en papel en zonas de acceso público, soportes con datos personales, etc.). Esta consideración incluye las pantallas que se utilicen para la visualización de imágenes del sistema de videovigilancia. Cuando se ausente del puesto de trabajo, se procederá al bloqueo de la pantalla o al cierre de la sesión.</li>
                            <li>Los documentos en papel y soportes electrónicos se almacenarán en lugar seguro (armarios o estancias de acceso restringido) durante las 24 horas del día.</li>
                            No se desecharán documentos o soportes electrónicos (cd, pen drives, discos duros, etc.) con datos personales sin garantizar su destrucción efectiva</li>
                            <li>No se comunicarán datos personales o cualquier otra información de carácter personal a terceros, prestando especial atención a no divulgar datos personales protegidos durante las consultas telefónicas, correos electrónicos, etc.</li>
                            <li>El deber de secreto y confidencialidad persiste incluso cuando finalice la relación laboral del trabajador con la empresa.</li>
                        </ul>

                        <h5>VIOLACIONES DE SEGURIDAD DE DATOS DE CARÁCTER PERSONAL</h5>
                        <p>Cuando se produzcan violaciones de seguridad de datos de carácter personal como, por ejemplo, el robo o acceso indebido a los datos personales se notificará a la Agencia Española de Protección de Datos en término de 72 horas acerca de dichas violaciones de seguridad, incluyendo toda la información necesaria para el esclarecimiento de los hechos que hubieran dado lugar al acceso indebido a los datos personales. La notificación se realizará por medios electrónicos a través de la sede electrónica de la Agencia Española de Protección de Datos en la dirección https://sedeagpd.gob.es/sede-electronica-web/</p>

                        <h5>MEDIDAS TÉCNICAS</h5>
                        <h6> IDENTIFICACIÓN</h6>

                        <ul>
                            <li>Cuando el mismo ordenador o dispositivo se utilice para el tratamiento de datos personales y fines de uso personal se recomienda disponer de varios perfiles o usuarios distintos para cada una de las finalidades. Deben mantenerse separados los usos profesional y personal del ordenador.</li>
                            <li>Se recomienda disponer de perfiles con derechos de administración para la instalación y configuración del sistema y usuarios sin privilegios o derechos de administración para el acceso a los datos personales. Esta medida evitará que en caso de ataque de ciberseguridad puedan obtenerse privilegios de acceso o modificar el sistema operativo.</li>
                            <li>Se garantizará la existencia de contraseñas para el acceso a los datos personales almacenados en sistemas electrónicos. La contraseña tendrá al menos 8 caracteres, mezcla de números y letras.</li>
                            <li>Cuando a los datos personales accedan distintas personas, para cada persona con acceso a los datos personales, se dispondrá de un usuario y contraseña específicos (identificación inequívoca).
                                • Se debe garantizar la confidencialidad de las contraseñas, evitando que queden expuestas a terceros. </li>
                            <li>Para la gestión de las contraseñas puede consultar la guía de privacidad y seguridad en internet de la Agencia Española de Protección de Datos y el Instituto Nacional de Ciberseguridad. En ningún caso se compartirán las contraseñas ni se dejarán anotadas en lugar común y el acceso de personas distintas del usuario.</li>
                        </ul>

                       <h5>DEBER DE SALVAGUARDA</h5>
                        <p>A continuación, se exponen las medidas técnicas mínimas para garantizar la salvaguarda de los datos personales:</p>

                        <ul>
                            <li>ACTUALIZACIÓN DE ORDENADORES Y DISPOSITIVOS: Los dispositivos y ordenadores utilizados para el almacenamiento y el tratamiento de los datos personales deberán mantenerse actualizados en la media posible.</li>
                            <li>MALWARE: En los ordenadores y dispositivos donde se realice el tratamiento automatizado de los datos personales se dispondrá de un sistema de antivirus que garantice en la medida posible el robo y destrucción de la información y datos personales. El sistema de antivirus deberá ser actualizado de forma periódica.</li>
                            <li>CORTAFUEGOS O FIREWALL: Para evitar accesos remotos indebidos a los datos personales se velará por garantizar la existencia de un firewall activado y correctamente configurado en aquellos ordenadores y dispositivos en los que se realice el almacenamiento y/o tratamiento de datos personales.</li>
                            <li>CIFRADO DE DATOS: Cuando se precise realizar la extracción de datos personales fuera del recinto donde se realiza su tratamiento, ya sea por medios físicos o por medios electrónicos, se deberá valorar la posibilidad de utilizar un método de encriptación para garantizar la confidencialidad de los datos personales en caso de acceso indebido a la información.</li>
                            COPIA DE SEGURIDAD: Periódicamente se realizará una copia de seguridad en un segundo soporte distinto del que se utiliza para el trabajo diario. La copia se almacenará en lugar seguro, distinto de aquél en que esté ubicado el ordenador con los ficheros originales, con el fin de permitir la recuperación de los datos personales en caso de pérdida de la información.</li>
                        </ul>
                      <p>Las medidas de seguridad serán revisadas de forma periódica, la revisión podrá realizarse por mecanismos automáticos (software o programas informáticos) o de forma manual. Considere que cualquier incidente de seguridad informática que le haya ocurrido a cualquier conocido le puede ocurrir a usted, y prevéngase contra el mismo.</p>

                        <p>Si desea más información u orientaciones técnicas para garantizar la seguridad de los datos personales y la información que trata su empresa, el Instituto Nacional de Ciberseguridad (INCIBE) en su página web www.incibe.es, pone a su disposición herramientas con enfoque empresarial en su sección «Protege tu empresa»   donde, entre otros servicios, dispone de:</p>

                        <ul>
                            <li>un apartado de formación con un videojuego, retos para respuesta a incidentes y videos interactivos de formación sectorial,</li>
                            <li>un Kit de concienciación para empleados,</li>
                            <li>diversas herramientas para ayudar a la empresa a mejorar su ciberseguridad, entre ellas políticas para el empresario, el personal técnico y el empleado, un catálogo de empresas y soluciones de seguridad y una herramienta de análisis de riesgos.</li>
                            <li>dosieres temáticos complementados con videos e infografías y otros recursos,</li>
                            <li>guías para el empresario,</li>
                        </ul>

                        <p>Además INCIBE, a través de la Oficina de Seguridad del Internauta, pone también a su disposición  herramientas informáticas gratuitas e información adicional pueden ser de utilidad para su empresa o su actividad profesional.</p>

                        <h6>CAPTACIÓN DE IMÁGENES CON CÁMARAS Y FINALIDAD DE SEGURIDAD</h6>
                        <p><strong> (VIDEOVIGILANCIA)</strong></p>
                        <p>
                            La imagen de una persona, en la medida que la identifique o la pueda identificar, constituye un dato de carácter personal que puede ser objeto de tratamiento para diversas finalidades. Si bien la más común consiste en utilizar las cámaras para garantizar la seguridad de personas, bienes e instalaciones, también pueden usarse con otros fines como el control de la prestación laboral de los trabajadores. A continuación, se incluyen las directrices básicas a respetar para que el tratamiento de las imágenes obtenidas a partir de cámaras de videovigilancia sea conforme a la normativa de protección de datos. No obstante, se recomienda la consulta de la Guía sobre el uso de videocámaras para seguridad y otras finalidades para un conocimiento más exhaustivo de las obligaciones que conlleva este tipo de tratamiento.
                        </p>

                        <p>• UBICACIÓN DE LAS CÁMARAS: Se evitará la captación de imágenes en zonas destinadas al descanso de los trabajadores, así como la captación de la vía pública si se utilizan cámaras exteriores, estando únicamente permitido la captación de la extensión mínima imprescindible para preservar la seguridad de las personas, bienes e instalaciones.</p>

                        <p> • UBICACIÓN DE MONITORES: Los monitores donde se visualicen las imágenes de las cámaras se ubicarán en un espacio de acceso restringido de forma que no sean accesibles a terceros. A las imágenes grabadas sólo accederá el personal autorizado.</p>
                        <p>CONSERVACIÓN DE IMÁGENES: Las imágenes se almacenarán durante el plazo máximo de un mes, con excepción de las imágenes que acrediten la comisión de actos que atenten contra la integridad de personas, bienes e instalaciones. En ese caso las imágenes deben ser puestas a disposición de la autoridad competente en un plazo de 72 horas desde que se tuviera conocimiento de la existencia de la grabación.</p>

                        <p>• DEBER DE INFORMACIÓN: Se informará acerca de la existencia de las cámaras y grabación de imágenes mediante un distintivo informativo colocado en un lugar suficientemente visible donde se identifique, al menos, la identidad del responsable y la posibilidad de los interesados de ejercer sus derechos en materia de protección de datos. En el propio pictograma se podrá incluir también un código de conexión o dirección de internet en la que se muestre esta información. Dispone de modelos, tanto del pictograma como del texto, en la página web de la Agencia.
                            ◦ Modelo de cartel de aviso de zona videovigilada.</p>

                        <p> • CONTROL LABORAL: Cuando las cámaras vayan a ser utilizadas con la finalidad de control laboral según lo previsto en el artículo 20.3 del Estatuto de los Trabajadores, se informará al trabajador y a sus representantes sindicales por cualquier medio que garantice la recepción de la información acerca de las medidas de control establecidas por el empresario con indicación expresa de la finalidad de control laboral de las imágenes captadas por las cámaras.</p>

                        <p> • DERECHO DE ACCESO A LAS IMÁGENES: Para dar cumplimiento al derecho de acceso de los interesados a las grabaciones del sistema de videovigilancia se solicitará una fotografía reciente y el Documento Nacional de Identidad del interesado para comprobar su identidad, así como el detalle de la fecha y hora a la que se refiere el derecho de acceso.</p>
                        <p> No se facilitará al interesado acceso directo a las imágenes de las cámaras en las que se muestren imágenes de terceros. En caso de no ser posible la visualización de las imágenes por el interesado sin mostrar imágenes de terceros, se le facilitará un documento en el que se confirme o niegue la existencia de imágenes del interesado.</p>
                        <p>Para más información puede consultar las guía y las fichas de videovigilancia y los informes jurídicos publicados por la Agencia Española de Protección de Datos en la sección de Videovigilancia.</p>

                    </div>
                    <div class="modal-footer">
<!--                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>-->
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Entendido</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
                crossorigin="anonymous"></script>

        <script>
            $(document).ready(function(){
               $('#close').click(function(e){
                   $('#cookies').hide(1500);
               });

                $('#cookie-accept').click(function(e){
                    let cookie = $('#cookie').val(true);
                    $.ajax({

                        url: 'controllers/cookies.php',
                        method: 'post',
                        data: cookie,
                        datatype: 'json',
                        statusCode: {
                            404: function() {
                                alert( "page not found" );
                            }
                        }
                    });

                    $('#cookies').hide(1500);
                });

                $('#cookie-close').click(function(e){
                    $('#cookies').hide(1500);
                });


            });
        </script>


    </body>
</html>
